import React from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Badge  } from 'reactstrap';

  

class Header extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  render() {
    return (
      <div>
        <Navbar color="light" light expand="md">
          <NavbarBrand href="/"><h1>Income</h1></NavbarBrand>
          <button><h2>Edit</h2></button>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink href="/components/">Saving Plan</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/components/">Reports</NavLink>
              </NavItem>
              <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                  View
                </DropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    Monthly
                  </DropdownItem>
                  <DropdownItem>
                    Weekly 
                  </DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem>
                    Yearly
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}
export default class Wallet extends React.Component {
    render() {
      return (
        <div>
          <h1>Wallet <Badge color="secondary">46583$</Badge></h1>
          < Header />
        </div>
      );
    }
  }