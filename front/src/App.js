import React, { Component } from 'react';
import './App.css';
import { withRouter, Switch, Route, Link } from "react-router-dom";
import { makeRequestUrl } from "./utils.js";
import * as auth0Client from './auth';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Header from './components/Header';
import Header2 from './components/Header2';
import Wallet from './components/Wallet';
import Wallet2 from './components/Wallet2';

const makeUrl = (path, params) => makeRequestUrl(`http://localhost:8080/${path}`,params)  


class App extends Component {
    state = { 
    items_list:[],
    error_message:"",
    isLoading: false,
    checkingSession:true
  }
  async componentDidMount() {
    this.getItemsList();
    if (this.props.location.pathname === "/callback") {
      this.setState({checkingSession:false});
      return;
    }
    try {
      await auth0Client.silentAuth();
      await this.getPersonalPageData(); 
            this.forceUpdate();
    } catch (err) {
      if (err.error !== "login_required") {
        console.log(err.error);
      }
    }
    this.setState({checkingSession:false});
  }

     getItemsList = async order => {
      try{
        const url = makeUrl(`items/list`, {order, token: this.state.token})
        const response = await fetch(url);
        
          const answer= await response.json()
          if(answer){
            const items_list = answer
            this.setState({items_list})
          }else{
            const error_message = answer.message 
            this.setState({error_message})
           }
        }catch(err){
          this.setState({error_message:err.message})
        }
      }
      getPersonalPageData = async () => {
        try {
          const url = makeUrl(`mypage`);
          const response = await fetch(url, {
            headers: { Authorization: `Bearer ${auth0Client.getIdToken()}` }
          });
          const answer = await response.json();
          if (answer.success) {
            const message = answer.result
            toast(`received from the server: '${message}'`);
          } else {
            this.setState({ error_message: answer.message });
            toast.error(
              `error message received from the server: ${answer.message}`
            );
          }
        } catch (err) {
          this.setState({ error_message: err.message });
          toast.error(err.message);
        }
      };
    
      renderUser() {
        const isLoggedIn = auth0Client.isAuthenticated()
        if (isLoggedIn) {
          return this.renderUserLoggedIn();
        } else {
          return this.renderUserLoggedOut();
        }
      }
      renderUserLoggedOut() {
        return (
          <button onClick={auth0Client.signIn}>Sign In</button>
        );
      }
      renderUserLoggedIn() {
        const nick = auth0Client.getProfile().name
        
        var list = this.state.items_list.map((item) => 
          <li>{item.id}</li>
        )
        return <div>
          Hello, {nick}! 
          <Header />
        <Wallet /> 
        <Header2 />
        <Wallet2 />
          <p>{list}</p>
          <button onClick={()=>{auth0Client.signOut();this.setState({})}}>logout</button>
        </div>
      }
  
    
      renderProfilePage = () => {
        if(this.state.checkingSession){
          return <p>validating session...</p>
        }
        return (
          <div>
            <p>profile page</p>
            {this.renderUser()}
          </div>
        );
      };
    
       
     
        renderContent() {
          if (this.state.isLoading) {
            return <p>loading...</p>;
          }
      
        return (
          <Switch>
            <Route path="/profile" render={this.renderProfilePage} />
            <Route path="/callback" render={this.handleAuthentication} />
            <Route render={() => <div>not found!</div>} />
          </Switch>
        );
      
        }

      isLogging = false;
      login = async () => {
        if (this.isLogging === true) {
          return;
        }
        this.isLogging = true;
        try{
          await auth0Client.handleAuthentication();
          const name = auth0Client.getProfile().name
          await this.getPersonalPageData() 
          toast(`${name} is logged in`)
          this.props.history.push('/profile')
        }catch(err){
          this.isLogging = false
          toast.error(err.message);
        }
      }
      handleAuthentication = ({history}) => {
        this.login(history)
        return <p>wait...</p>
      }
    
      render() {
        return (
          <div className="App">
            <div>
            <Link to="/profile">sign in</Link> 
           
              </div>
           {this.renderContent()}
            <ToastContainer />
            </div>
        );
      }
    }
    

export default withRouter(App);
