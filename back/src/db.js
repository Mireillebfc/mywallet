import sqlite from 'sqlite'
import SQL from 'sql-template-strings';

const initializeDatabase = async () => {

  const db = await sqlite.open('./db.sqlite');
//await db.migrate({ force: 'last' })

  
 
  const getItemsList = async () => {
    const rows = await db.all("SELECT item_id AS id FROM test")
    return rows
  }

  
  const controller = {
    getItemsList
  }

  return controller
}

export default initializeDatabase
