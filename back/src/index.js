import app from './app'
import initializeDatabase from './db'
import { isLoggedIn } from './auth'

const start = async () => {
  const controller = await initializeDatabase()
  app.get('/', (req, res) => res.send("ok"));

  app.get('/items/list', async (req, res) => {
    const items_list = await controller.getItemsList()
    res.json(items_list)
  })

  app.get('/mypage', isLoggedIn, ( req, res ) => {
    const username = req.user.name
    res.send({success:true, result: 'ok, user '+username+' has access to this page'})
  })

  
  app.listen(8080, () => console.log('server listening on port 8080'))
}
start();
