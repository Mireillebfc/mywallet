--------------------------------------------------------------------------------
-- Up
--------------------------------------------------------------------------------
CREATE TABLE users (
    user_id INTEGER PRIMARY KEY AUTOINCREMENT,
    auth0_sub TEXT UNIQUE, 
    nickname TEXT    
);

INSERT INTO users (auth0_sub, nickname) VALUES ("fakeId","fake@fake.com");

ALTER TABLE test RENAME TO old_test

CREATE TABLE test (
    item_id INTEGER PRIMARY KEY AUTOINCREMENT,
    author_id TEXT,
    FOREIGN KEY(author_id) REFERENCES users(auth0_sub)
);

INSERT INTO test (author_id) VALUES ("fakeId");

--------------------------------------------------------------------------------
-- Down
--------------------------------------------------------------------------------
DROP TABLE users

ALTER TABLE test RENAME TO old_test

CREATE TABLE test (
    item_id INTEGER PRIMARY KEY AUTOINCREMENT,
    );


DELETE FROM old test WHERE author_id = "fakeId";


INSERT INTO test (item_id) SELECT item_id FROM test;

DROP TABLE old_test;
